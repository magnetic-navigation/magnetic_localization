# Magnetic Localization

This is the repo for magnetic localization and fusion algorithms, all the ROS2 packages that were developed.

This work was carried out by Sudarshan Srinivasa Rangan while at Avular(2022), under the supervision of Mr. Pasquale Van Heumen (Avular).
University supervisor - Dr.ir.Ion Barosan (TU/e), Dr.ir.Eugene Lepelaars (TNO)

This work was a part of master thesis project towards MSc Embedded Systems at TU Eindhoven.

Please cite this work if you use it in your research
