# Custom ROS2 Message type for magnetic localization

This message type is for use in the fusion node that implements weighted averaging

## Message structure

    std_msgs/Header header
    geometry_msgs/Pose[] poses
    float64[] weights

Here weights as been added to communicate the weights for possible locations.
