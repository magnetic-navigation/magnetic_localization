from setuptools import setup

package_name = 'knn_localization'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Sudarshan',
    maintainer_email='s.rangan@avular.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'knn_localization = knn_localization.knn_localization:main',
            'knn_localization_weights = knn_localization.knn_localization_weights:main',
            'knn_avg_localization_mag = knn_localization.knn_avg_localization_mag:main',
            'knn_wt_avg_localization_mag = knn_localization.knn_wt_avg_localization_mag:main',
            # 'knn_threshold_localization = knn_localization.knn_threshold_localization:main',
            # 'knn_normal_avg_loc_with_weights = knn_localization.knn_localization_weights_normal_avg:main'

        ],
    },
)
