import rclpy
from rclpy.node import Node

import statistics
import pandas as pd
import numpy as np

from sensor_msgs.msg import MagneticField  # the message for magnetic field data

from geometry_msgs.msg import Pose
# from geometry_msgs.msg import PoseArray
# the message for sending possible locations along with weights
# from maglocmsg.msg import MagLocMsg
# from custom_msgs.msg import MagLoc

# K value for KNN

K_value = 5


sampleX = 0
sampleY = 0
sampleZ = 0


class magSubscriber(Node):

    def __init__(self):
        super().__init__('weighted_KNN_node')

        self.subscription = self.create_subscription(
            MagneticField, 'mag_vals', self.mag_callback, 10)

        # self.publisher_ = self.create_publisher(
        #     PoseArray, 'mag_pose_array', 10)
        # self.publisher_ = self.create_publisher(
        #     MagLocMsg, 'mag_pose_array', 10)

        timer_period = 0.2  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

        self.declare_parameter(
            'magnetic_map', 'magnetic_map.csv')

        filename = self.get_parameter(
            'magnetic_map').get_parameter_value().string_value

        self.publisher_ = self.create_publisher(
            Pose, 'mag_pose', 10)

        # Load the map data for all 3 axes here
        #  load the csv file and load all the data frames

        global df, weightX, weightY, weightZ
        df = pd.DataFrame()

        df_mag_map = pd.read_csv(filename)
        df = df_mag_map.copy()

        # df_mag_map = pd.read_csv(filename)
        # df = df_mag_map.copy()

        meanX = df['mag_x'].mean()
        meanY = df['mag_y'].mean()
        meanZ = df['mag_z'].mean()

        print("meanX = {}, meanY = {}, meanZ = {} ".format(meanX, meanY, meanZ))

        stdX = df['mag_x'].std()
        stdY = df['mag_y'].std()
        stdZ = df['mag_z'].std()

        print("stdX = {}, stdY = {}, stdZ = {} ".format(stdX, stdY, stdZ))

        tot_Weights = stdX + stdY + stdZ

        weightX = stdX/tot_Weights
        weightY = stdY/tot_Weights
        weightZ = stdZ/tot_Weights

        print("WeightX = {}, WeightY = {}, WeightZ = {}" .format(
            weightX, weightY, weightZ))

    def KNN_calc(self):

        _logger = self.get_logger()
        # self.get_logger().info('KNN calculation')
        # print("KNN calculation")
        _logger.info("KNN calculation")

        # #################################!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # compute the distance of the sample compared to all the points in the map
        # replace valXYZ, with individual values from the 2D map

        distance = weightX*(sampleX - df['mag_x'])**2 + weightY*(
            sampleY - df['mag_y'])**2 + weightZ*(sampleZ - df['mag_z'])**2

        # print(distance)
        # find out K positions with the smallest distance here
        # dist_Ksmallest = distance.nsmallest(K_value)
        # # by default takes the first occurence incase of repeated values

        dist_Ksmallest = distance.nsmallest(K_value, keep="all")
        # keeps all repeated values within K_value limit
        _logger.info(f"Ksmallest{dist_Ksmallest}")
        # print(dist_Ksmallest)

        # calculate the weights for the points based on distance here and return it as an list
        weights = []
        # weights = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]
        dist_min, dist_max = min(dist_Ksmallest), max(dist_Ksmallest)
        start_normalize = 0.1
        end_normalize = 1
        width_normalize = end_normalize - start_normalize

        # print(dist_min, dist_max)
        _logger.info(f"dist_min = {dist_min}, dist_max = {dist_max}")

        for i, val in enumerate(dist_Ksmallest):
            weights.append(1-(((val-dist_min) / (dist_max-dist_min))
                           * width_normalize))

        index_di = dist_Ksmallest.index
        output_df = df.loc[index_di]

        # print(weights)
        _logger.info(f"WEIGHTS = {weights}")
        # print(output_df)

        output_df = output_df.filter(['loc_x', 'loc_y'], axis=1)
        # _logger.info(f"output_df: {output_df}")

        return output_df, weights

    def mag_callback(self, msg):
        # self.get_logger().info('I heard: text')
        # _logger = self.get_logger()
        # _logger.info("OOLALA")
        global sampleX, sampleY, sampleZ
        sampleX = msg.magnetic_field.x
        sampleY = msg.magnetic_field.y
        sampleZ = msg.magnetic_field.z
        self.get_logger().info('x = {:.2}, y = {:.2}, z = {:.2}' .format(
            msg.magnetic_field.x, msg.magnetic_field.y, msg.magnetic_field.z))
        # execute KNN here, everytime new data is obtained

    def timer_callback(self):
        # call the KNN calculation and publish the pose array
        _logger = self.get_logger()

        output_df, weights = self.KNN_calc()

        # Create the Pose() and PoseArray() carriers

        # pose_array = PoseArray()
        # pose_array = MagLocMsg()
        # pose_array = MagLoc()
        # pose_list = []

        # get the index from nsmallest and use it here
        # Iterate over each row
        i = 0
        x_sum = 0.0
        y_sum = 0.0
        weights_sum = 0.0

        for rows in output_df.itertuples():
            x_sum += weights[i] * rows.loc_x
            y_sum += weights[i] * rows.loc_y
            i += 1

            # perform averaging in this for loop

        weights_sum = sum(weights)
        initial_pose_x = x_sum/weights_sum
        initial_pose_y = y_sum/weights_sum

        # Print the list
        # _logger.info("pose array formed")
        # print("pose array")
        # print(pose_array)

        initial_pose = Pose()
        initial_pose.position.x = initial_pose_x
        initial_pose.position.y = initial_pose_y

        self.publisher_.publish(initial_pose)

        _logger.info("POSE_X = {}, POSE_Y = {}".format(
            initial_pose_x, initial_pose_y))
        _logger.info("published pose")


def main(args=None):

    rclpy.init(args=args)

    mag_sub = magSubscriber()

    rclpy.spin(mag_sub)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    mag_sub.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
