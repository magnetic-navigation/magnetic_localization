import rclpy
from rclpy.node import Node

import statistics
import pandas as pd
import numpy as np

from sensor_msgs.msg import MagneticField  # the message for magnetic field data

from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray

# K value for KNN

K_value = 9

# magnetic THRESHOLD VALUE

mag_threshold_value = 0.5

sampleX = 0
sampleY = 0
sampleZ = 0


class magSubscriber(Node):

    def __init__(self):
        super().__init__('threshold_KNN_node')

        self.subscription = self.create_subscription(
            MagneticField, 'mag_vals', self.mag_callback, 10)

        self.publisher_ = self.create_publisher(
            PoseArray, 'mag_pose_array', 10)
        timer_period = 0.2  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

        self.declare_parameter(
            'magnetic_map', '/home/sudar/Desktop/TurtleVM_shared_folder/magnetic_localization/magnetic_maps/mag_maps_interpolated_blore_bdroom.csv')

        filename = self.get_parameter(
            'magnetic_map').get_parameter_value().string_value

        # Load the map data for all 3 axes here
        #  load the csv file and load all the data frames
        # filename = 'mag_maps_interpolated_blore_bdroom.csv'
        global df, weightX, weightY, weightZ
        df = pd.DataFrame()

        df_mag_map = pd.read_csv(filename)
        df = df_mag_map.copy()

        # path = ''
        # filename = 'mag_maps_interpolated_blore_bdroom.csv'

        # df_mag_map = pd.read_csv(filename)
        # df = df_mag_map.copy()

        meanX = df['mag_x'].mean()
        meanY = df['mag_y'].mean()
        meanZ = df['mag_z'].mean()

        print("meanX = {}, meanY = {}, meanZ = {} ".format(meanX, meanY, meanZ))

        stdX = df['mag_x'].std()
        stdY = df['mag_y'].std()
        stdZ = df['mag_z'].std()

        print("stdX = {}, stdY = {}, stdZ = {} ".format(stdX, stdY, stdZ))

        tot_Weights = stdX + stdY + stdZ

        weightX = stdX/tot_Weights
        weightY = stdY/tot_Weights
        weightZ = stdZ/tot_Weights

        print("WeightX = {}, WeightY = {}, WeightZ = {}" .format(
            weightX, weightY, weightZ))

    def KNN_calc(self):

        _logger = self.get_logger()
        # self.get_logger().info('KNN calculation')
        print("KNN calculation")
        _logger.info("KNN calculation")

        # #################################!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # compute the distance of the sample compared to all the points in the map
        # replace valXYZ, with individual values from the 2D map

        distance = weightX*(sampleX - df['mag_x'])**2 + weightY*(
            sampleY - df['mag_y'])**2 + weightZ*(sampleZ - df['mag_z'])**2

        # print(distance)

        # threshold_df = distance < mag_threshold_value
        # threshold_df = distance.index[distance > mag_threshold_value]

        # check for values that are smaller than the threshold and then get their locations
        # based on the norm value computed by the euclidean distance metric
        threshold_df = df.loc[distance.index[distance < mag_threshold_value]].filter([
                                                                                     'loc_x', 'loc_y'], axis=1)

        # print(threshold_df)

        # check if the length of threshold_df is smaller than K_value then return the K smallest values
        # otherwise return the threshold_df

        if(len(threshold_df['loc_x']) > K_value):
            return threshold_df

        else:
            # find out K positions with the smallest distance here
            # dist_Ksmallest = distance.nsmallest(K_value)
            # by default takes the first occurence incase of repeated values

            dist_Ksmallest = distance.nsmallest(K_value, keep="all")
            # keeps all repeated values within K_value limit
            # print("Ksmallest")
            # print(dist_Ksmallest)
            # print()

            index_di = dist_Ksmallest.index
            output_df = df.loc[index_di]
            # print(output_df)

            output_df = output_df.filter(['loc_x', 'loc_y'], axis=1)
            print(output_df)

            return output_df

    def mag_callback(self, msg):
        # self.get_logger().info('I heard: text')
        # _logger = self.get_logger()
        # _logger.info("OOLALA")
        global sampleX, sampleY, sampleZ
        sampleX = msg.magnetic_field.x
        sampleY = msg.magnetic_field.y
        sampleZ = msg.magnetic_field.z
        self.get_logger().info('x = {:.2}, y = {:.2}, z = {:.2}' .format(
            msg.magnetic_field.x, msg.magnetic_field.y, msg.magnetic_field.z))
        # execute KNN here, everytime new data is obtained

    def timer_callback(self):
        # call the KNN calculation and publish the pose array
        _logger = self.get_logger()

        output_df = self.KNN_calc()

        # Create the Pose() and PoseArray() carriers

        pose_array = PoseArray()
        pose_list = []

        # get the index from nsmallest and use it here
        # Iterate over each row
        for rows in output_df.itertuples():
            pose_individual = Pose()
            pose_individual.position.x = rows.loc_x
            pose_individual.position.y = rows.loc_y

            # print("IndividualPose")
            # print(pose_individual)
            # append pose to pose_aray here
            # pose_array.poses.append(pose_individual)
            pose_list.append(pose_individual)
            pose_array.poses = pose_list
            # pose_array.poses.insert(len(pose_array.poses), pose_individual)

            # print("PoseArray")
            # print(pose_array)

        # print("PoseArray")
        # print(pose_array)

        # Print the list
        _logger.info("pose array formed")
        # print("pose array")
        # print(pose_array)

        self.publisher_.publish(pose_array)

        _logger.info("published pose array")


def main(args=None):

    rclpy.init(args=args)

    mag_sub = magSubscriber()

    rclpy.spin(mag_sub)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    mag_sub.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
