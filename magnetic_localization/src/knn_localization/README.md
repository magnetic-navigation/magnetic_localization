# KNN based magnetic localization

Uses the Euclidean distance metric to find values that are similar to the test value input to the algorithm. Then it outputs K possible points.


# KNN based magnetic localization with threshold parameter

In addition the the K parameter, we also add an threshold parameter to the localization node. This can be useful in cases where we want more values to be output by the magnetic localization node.

### Hyperparameters

* K_value - K values that are to be returned
* mag_threshold_value -> threshold value specified, to consider locations with norm values that meet this threshold

The script returns all the possible locations that meets the mag_threshold_value, if not at least K_values.


# KNN node with weights output

This node uses a custom ROS2 message type to send the weights based on distances along with the possible locations


# Other KNN node
Is an implementation of normal average using the weighted average node