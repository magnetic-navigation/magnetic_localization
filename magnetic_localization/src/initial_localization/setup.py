from setuptools import setup

package_name = 'initial_localization'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='sudar',
    maintainer_email='s.rangan@avular.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'knn_localization = initial_localization.knn_localization:main',
            'knn_localization_weights = initial_localization.knn_localization_weights:main',
            # 'knn_threshold_localization = initial_localization.knn_threshold_localization:main',
        ],
    },
)
