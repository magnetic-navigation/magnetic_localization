import rclpy
from rclpy.node import Node

# msg needed for /scan.
from sensor_msgs.msg import LaserScan

import statistics
import pandas as pd
import numpy as np

# msgs needed for /cmd_vel.
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3

from sensor_msgs.msg import MagneticField  # the message for magnetic field data

# import Quality of Service library, to set the correct profile and reliability.
from rclpy.qos import ReliabilityPolicy, QoSProfile

sampleX = 0
sampleY = 0
sampleZ = 0

first_value_x = 0
first_value_y = 0
first_value_z = 0

rotated_value_x = 0
rotated_value_y = 0
rotated_value_z = 0

count = 0


class orientationAlignement(Node):
    # Constructor parameters
    #   k_p_ang:         proportional control factor controlling how much the error in
    #                      angle affects the angular velocity
    def __init__(self, k_p_ang=0.004):
        super().__init__('orientationAlignement_Node')

        self.subscription = self.create_subscription(
            LaserScan, 'scan', self.process_scan,
            QoSProfile(depth=5, reliability=ReliabilityPolicy.BEST_EFFORT))

        # self.subscription = self.create_subscription(
        #     LaserScan, 'scan', self.process_scan, 10)

        # self.subscription = self.create_subscription(
        #     MagneticField, 'mag_vals', self.mag_callback, 10)

        self.twist_pub = self.create_publisher(
            Twist, 'cmd_vel', 10)

        # Create a default twist msg (all values 0).
        lin = Vector3()
        ang = Vector3()
        self.twist = Twist(linear=lin, angular=ang)

        # Initialize constructor parameters.
        self.k_p_ang = k_p_ang

        # record the mag values first here
        global first_value_x, first_value_y, first_value_z

        first_value_x = sampleX
        first_value_y = sampleY
        first_value_z = sampleZ

        # timer_period = 0.2  # in seconds; 5Hz
        # self.timer = self.create_timer(timer_period, self.process_scan)

    # def run_orientation_estimate(self):

    #     _logger = self.get_logger()

    #     global rotated_value_x, rotated_value_y, rotated_value_z

    #     rotated_value_x = sampleX
    #     rotated_value_y = sampleY
    #     rotated_value_z = sampleZ

    #     _logger.info("initx = {}, inity={}, initz={}"
    #                  .format(first_value_x, first_value_y, first_value_z))

    #     _logger.info("rotx = {}, roty={}, rotz={}"
    #                  .format(rotated_value_x, rotated_value_y, rotated_value_z))

    # Determine nearest object by looking at scan data from all angles
    #   the robot, set velocity based on that information, and
    #   publish to cmd_vel.

    def process_scan(self, data):

        _logger = self.get_logger()
        _logger.info("objectAlignment")

        # Initialize nearest_index and nearest_distance, which will keep
        #   track of the angle and distance to the object nearest to the
        #   robot.
        nearest_index = -1
        # Maximum LiDAR range is 3m, so init value is safe.
        nearest_distance = 10

        # Iterate through data.ranges, which contains the nearest object
        #   at each degree increment. If the value is non-zero, there is
        #   an object in that direction. Use only indices (70, 110) to avoid
        #   LiDAR detecting the robot arm.
        for i in range(70, 110):
            if data.ranges[i] > 0.0 and data.ranges[i] < nearest_distance:
                nearest_index = i
                nearest_distance = data.ranges[i]

        # If no object was found within the LiDAR range, stop the robot.
        if (nearest_distance == 10):
            self.twist.linear.x = 0
            self.twist.angular.z = 0
            self.twist_pub.publish(self.twist)
            return

        # Calculate the discrepancy between the robot's angle and the desired angle (90 degrees).
        error_angle = nearest_index - 90

        # If we are aligned, stop the robot and shut down.
        if error_angle == 0:
            print("Aligned now!")
            self.twist.linear.x = 0.0
            self.twist.angular.z = 0.0
            self.twist_pub.publish(self.twist)

            # self.run_orientation_estimate()

            global count
            count = count + 1
            if(count == 7):
                # if(count == 3):
                print("Aligned!")
                exit(0)

        # Set velocity based on the proportional control mechanism.
        self.twist.angular.z = self.k_p_ang * error_angle

        # Clamp angular speed to 1.5 rad/s to ensure we don't exceed the maximum velocity of the Turtlebot.
        self.twist.angular.z = min(1.5, max(self.twist.angular.z, -1.5))

        # Publish Twist message to cmd_vel.
        self.twist_pub.publish(self.twist)

    def mag_callback(self, msg):

        global sampleX, sampleY, sampleZ
        sampleX = msg.magnetic_field.x
        sampleY = msg.magnetic_field.y
        sampleZ = msg.magnetic_field.z
        # self.get_logger().info('x = {:.2}, y = {:.2}, z = {:.2}' .format(
        #     msg.magnetic_field.x, msg.magnetic_field.y, msg.magnetic_field.z))


def main(args=None):

    rclpy.init(args=args)

    alignment = orientationAlignement()

    rclpy.spin(alignment)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    alignment.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
