
# Script for fusion matching strategy. Contains of subscribers to amcl and mag poses.
# Further performing the fusion by averaging in this script

# The matching strategy checks for matches between amcl and magnetic poses instead of just taking an average
from collections import deque
import rclpy
from rclpy.node import Node

import sys
import math
from scipy.spatial import distance

import statistics
import pandas as pd
import numpy as np

from sensor_msgs.msg import MagneticField  # the message for magnetic field data

from geometry_msgs.msg import Pose, PoseArray, PoseWithCovarianceStamped


# Base class to handle exceptions
from tf2_ros import TransformException

# Stores known frames and offers frame graph requests
from tf2_ros.buffer import Buffer

# Easy way to request and receive coordinate frame transform
from tf2_ros.transform_listener import TransformListener

mag_pose_array = []

# for reloczlization
list_relocalize = deque([0, 0, 0, 0, 0])

# create the subscribers here


class fusionSubscriber(Node):

    def __init__(self):
        super().__init__('fusion_Subscriber')

        # mag_pose_array subscriber here
        self.subscription = self.create_subscription(
            PoseArray, 'mag_pose_array', self.mag_pose_array_callback, 10)

        # amcl transform listener
        # Frame switch between base_link and rm3100_mag
        self.frame_switch = 1

        # Declare and acquire `target_frame` parameter
        self.declare_parameter('target_frame', 'base_link')
        self.target_frame = self.get_parameter(
            'target_frame').get_parameter_value().string_value

        self.from_frame = 'map'
        self.to_frame = self.target_frame

        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)

        self.tf_buffer2 = Buffer()
        self.tf_listener2 = TransformListener(self.tf_buffer2, self)

        # publisher for final pose
        self.publisher_ = self.create_publisher(
            Pose, 'fused_pose', 10)

        # timer_period = 0.5 # normal timer period
        timer_period = 2.5  # use this for relocalization

        self.timer = self.create_timer(timer_period, self.on_timer)

    def mag_pose_array_callback(self, msg):
        # get the mag poses here
        global mag_pose_array

        mag_pose_array = msg.poses
        # self.get_logger().info("Pose Array obtained")

    # subscriber for amcl_pose topic
    # self.subscription = self.create_subscription(
    #     PoseWithCovarianceStamped, 'amcl_pose', self.amcl_callback, 10)
    # self.subscription  # prevent unused variable warning

    def on_timer(self):
        now_tf2 = rclpy.time.Time()
        trans = None
        trans2 = None
        # for testing, remove this statement otherwise
        # self.decision_fusion(trans)
        try:
            if(self.frame_switch):

                trans = self.tf_buffer.lookup_transform(
                    'map',
                    'base_link',
                    now_tf2)

            else:
                trans = self.tf_buffer.lookup_transform(
                    'map',
                    'rm3100_mag',
                    now_tf2)
                # rm3100_mag frame

            _logger = self.get_logger()

            self.decision_fusion(trans)

        except TransformException as ex:
            self.get_logger().info(
                f'Could not transform {self.from_frame} to {self.to_frame}: {ex}')
            return

        # get the amcl pose here

    def decision_fusion(self, trans):
        # perform the fusion here

        self.get_logger().info("decision_fusion")
        # avg the the mag pose PoseArray
        # mag_loc_x, mag_loc_y = self.magnetic_avg(mag_pose_array)

        amcl_loc_x = trans.transform.translation.x
        amcl_loc_y = trans.transform.translation.y

        # for testing
        # amcl_loc_x = 0.7
        # amcl_loc_y = -0.7
        # print(amcl_loc_x, amcl_loc_y)
        self.get_logger().info(
            "amcl_loc_x = {:.4}, amcl_loc_y = {:.4}".format(amcl_loc_x, amcl_loc_y))
        # finally avg both amcl loc and mag loc

        # final_loc_x, final_loc_y = self.fuse_avg(
        # amcl_loc_x, amcl_loc_y, mag_loc_x, mag_loc_y)

        final_loc_x, final_loc_y = self.fuse_match(
            amcl_loc_x, amcl_loc_y, mag_pose_array)

        # self.get_logger().info(
        #     "final_loc_x = {:.4}, final_loc_y = {:.4}".format(final_loc_x, final_loc_y))

        # publish this as the final pose

        fused_pose = Pose()
        fused_pose.position.x = final_loc_x
        fused_pose.position.y = final_loc_y

        self.publisher_.publish(fused_pose)

        # self.get_logger().info("Published Fused Pose")

    def fuse_match(self, amcl_loc_x, amcl_loc_y, mag_pose_array):
        # fuse based on matching the poses received from amcl and magnetometer localization
        # Using euclidean distance for matching??
        mag_list = []
        for i in range(len(mag_pose_array)):
            list_mag = []
            x_val = mag_pose_array[i].position.x
            y_val = mag_pose_array[i].position.y
            list_mag = [x_val, y_val]
            mag_list.append(list_mag)

        K = 1

        # def pClosest(points, K):

        #     points.sort(key=lambda K: K[0]**2 + K[1]**2)

        #     return points[:K]

        # mag_values = pClosest(mag_list, K)

        mag_list.sort(key=lambda K: K[0]**2 + K[1]**2)

        mag_values = mag_list[:K]

        # check if the smallest value is above a threshold, if yes then put relocalize message and quit
        global list_relocalize

        def check_relocalize():
            threshold_relocalize = 0.5  # 50 cm is the threshold
            # self.get_logger().info(
            #     "dist value = {:.4}".format(mag_list[0]))
            # print("maglist", mag_list)
            ecu_dist_thres = distance.euclidean(
                mag_list[0], [amcl_loc_x, amcl_loc_y])

            # append to the left and pop from the right everytime we check
            if(ecu_dist_thres > threshold_relocalize):
                list_relocalize.appendleft(1)
                list_relocalize.pop()
                self.get_logger().info(
                    "Exceeded THRESHOLD")
            else:
                list_relocalize.appendleft(0)
                list_relocalize.pop()

            if(sum(list_relocalize) == 5):
                self.get_logger().info(
                    "!!! RELOCALIZE THE ROBOT !!!")
                sys.exit()

        check_relocalize()
        # self.get_logger().info(
        #     "mag_loc_x = {:.4}, mag_loc_y = {:.4}".format(mag_values[0][0], mag_values[0][1]))
        # print(mag_values[0][0])

        # find closest points to each test point

        # euclidean dist of all points with one another

        # average euclidean dist for each point w.r.t. the no. of closest points

        # make a decision on what point to use - use the point directly or make an avg using its closest points

        # TODO: assign the return values - use a different logic than averaging and remove this TODO
        final_loc_x = (amcl_loc_x + mag_values[0][0])/2
        final_loc_y = (amcl_loc_y + mag_values[0][1])/2

        return final_loc_x, final_loc_y

    # TODO: remove this function
    # def magnetic_avg(self, mag_pose_array):
    #     # perform averaging of the magnetic poses

    #     x_sum = 0.0
    #     y_sum = 0.0
    #     # print(mag_pose_array)
    #     for i in range(len(mag_pose_array)):

    #         x_sum += mag_pose_array[i].position.x
    #         y_sum += mag_pose_array[i].position.y

    #     mag_loc_x = x_sum/len(mag_pose_array)

    #     mag_loc_y = y_sum/len(mag_pose_array)

        # self.get_logger().info(
        #     "mag_loc_x = {:.4}, mag_loc_y = {:.4}".format(mag_loc_x, mag_loc_y))

    #     return mag_loc_x, mag_loc_y


def main(args=None):

    rclpy.init(args=args)

    fuse_sub = fusionSubscriber()

    rclpy.spin(fuse_sub)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    fuse_sub.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
