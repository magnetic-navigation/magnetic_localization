# Script for fusion basic strategy. Contains of subscribers for amcl and mag poses.
# Further performing the fusion by !!MAGNETIC ONLY WEIGHTED!! averaging in this script
import rclpy
from rclpy.node import Node

import statistics
import pandas as pd
import numpy as np

from sensor_msgs.msg import MagneticField  # the message for magnetic field data

from geometry_msgs.msg import Pose, PoseArray, PoseWithCovarianceStamped

# the message for sending possible locations along with weights
from maglocmsg.msg import MagLocMsg


# Base class to handle exceptions
from tf2_ros import TransformException

# Stores known frames and offers frame graph requests
from tf2_ros.buffer import Buffer

# Easy way to request and receive coordinate frame transform
from tf2_ros.transform_listener import TransformListener

mag_pose_array, weights = [], []

# create the subscribers here


class fusionSubscriber(Node):

    def __init__(self):
        super().__init__('fusion_Subscriber')

        # mag_pose_array subscriber here
        self.subscription = self.create_subscription(
            MagLocMsg, 'mag_pose_array', self.mag_pose_array_callback, 10)

        # amcl transform listener
        # Frame switch between base_link and rm3100_mag
        self.frame_switch = 1

        # Declare and acquire `target_frame` parameter
        self.declare_parameter('target_frame', 'base_link')
        self.target_frame = self.get_parameter(
            'target_frame').get_parameter_value().string_value

        self.from_frame = 'map'
        self.to_frame = self.target_frame

        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)

        self.tf_buffer2 = Buffer()
        self.tf_listener2 = TransformListener(self.tf_buffer2, self)

        # publisher for final pose
        self.publisher_ = self.create_publisher(
            Pose, 'fused_pose', 10)

        timer_period = 0.5
        self.timer = self.create_timer(timer_period, self.on_timer)

    def mag_pose_array_callback(self, msg):
        # get the mag poses here
        # global mag_pose_array, weights
        global mag_pose_array

        # mag_pose_array = msg.poses
        # weights = msg.weights
        mag_pose_array = msg
        # print(mag_pose_array)

        self.get_logger().info("Pose Array obtained")

    # subscriber for amcl_pose topic
    # self.subscription = self.create_subscription(
    #     PoseWithCovarianceStamped, 'amcl_pose', self.amcl_callback, 10)
    # self.subscription  # prevent unused variable warning

    def on_timer(self):
        now_tf2 = rclpy.time.Time()
        trans = None
        trans2 = None

        try:
            # for testing purposes, do not uncomment this line
            # self.decision_fusion(trans)
            if(self.frame_switch):
                # self.decision_fusion()  # for testing, remove this statement otherwise
                trans = self.tf_buffer.lookup_transform(
                    'map',
                    'base_link',
                    now_tf2)

            else:
                trans = self.tf_buffer.lookup_transform(
                    'map',
                    'rm3100_mag',
                    now_tf2)
                # rm3100_mag frame

            _logger = self.get_logger()

            self.decision_fusion(trans)

        except TransformException as ex:
            self.get_logger().info(
                f'Could not transform {self.from_frame} to {self.to_frame}: {ex}')
            return

    def decision_fusion(self, trans):
        # perform the fusion here

        self.get_logger().info("decision_fusion")
        # avg the the mag pose PoseArray
        mag_loc_x, mag_loc_y = self.magnetic_avg(mag_pose_array)

        amcl_loc_x = trans.transform.translation.x
        amcl_loc_y = trans.transform.translation.y

        # for testing
        # amcl_loc_x = 10.0
        # amcl_loc_y = 1.0
        # print(amcl_loc_x, amcl_loc_y)
        self.get_logger().info(
            "amcl_loc_x = {:.4}, amcl_loc_y = {:.4}".format(amcl_loc_x, amcl_loc_y))
        # finally avg both amcl loc and mag loc

        final_loc_x, final_loc_y = self.fuse_avg(
            amcl_loc_x, amcl_loc_y, mag_loc_x, mag_loc_y)

        self.get_logger().info(
            "final_loc_x = {:.4}, final_loc_y = {:.4}".format(final_loc_x, final_loc_y))

        # publish this as the final pose

        fused_pose = Pose()
        fused_pose.position.x = final_loc_x
        fused_pose.position.y = final_loc_y

        self.publisher_.publish(fused_pose)

        self.get_logger().info("Published Fused Pose")

    def fuse_avg(self, amcl_loc_x, amcl_loc_y, mag_loc_x, mag_loc_y):

        weight_avg = 0.5
        # equal weights

        final_loc_x = weight_avg*(amcl_loc_x + mag_loc_x)/2
        final_loc_y = (1 - weight_avg)*(amcl_loc_y + mag_loc_y)/2

        return final_loc_x, final_loc_y

    def magnetic_avg(self, mag_pose_array):
        # perform averaging of the magnetic poses

        x_sum = 0.0
        y_sum = 0.0
        weights_sum = 0.0
        # print(mag_pose_array)
        for i in range(len(mag_pose_array.poses)):

            weights_sum += mag_pose_array.weights[i]

            x_sum += mag_pose_array.weights[i] * \
                mag_pose_array.poses[i].position.x

            y_sum += mag_pose_array.weights[i] * \
                mag_pose_array.poses[i].position.y

        print(x_sum, y_sum, weights_sum)

        mag_loc_x = x_sum/weights_sum

        mag_loc_y = y_sum/weights_sum

        self.get_logger().info(
            "mag_loc_x = {:.4}, mag_loc_y = {:.4}".format(mag_loc_x, mag_loc_y))

        return mag_loc_x, mag_loc_y


def main(args=None):

    rclpy.init(args=args)

    fuse_sub = fusionSubscriber()

    rclpy.spin(fuse_sub)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    fuse_sub.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
