from setuptools import setup

package_name = 'fusion_basic'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Sudarshan',
    maintainer_email='s.rangan@avular.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'fusion_basic = fusion_basic.fusion_basic:main',
            # 'fusion_avg_using_weighted_avg = fusion_basic.fusion_avg_using_weighted_avg:main'
        ],
    },
)
